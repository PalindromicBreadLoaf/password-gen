A stupidly simple password generator shell script.

This script generates 10 25-character passwords by default, but changing that is as simple as modifying two numbers in the script.
I may eventually change it to just ask, but for now I was too lazy.

Running it is as simple as:
Cloning this repo
```
git clone https://gitlab.com/PalindromicBreadLoaf/password-gen
```
Moving into the cloned directory
```
cd password-gen
```
Making the file executable
```
sudo chmod +x password-gen
```
And running the file
```
./password-gen
```

I wrote this in all of 3 minutes and it uses dd, so if this breaks your entire computer it isn't my fault.
